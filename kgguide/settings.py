# -*- coding: utf-8 -*-

import os
import sys
from uuid import uuid4
from pytz import timezone

from settings_private import *

sys.path.insert(0, os.path.join(BASE_DIR, "plugins"))

ADMIN_WYSIWIG_UPLOADS_DIR = 'blog_posts/'

APPEND_SLASH = True

BANNER_POSITIONS = (
    ('1', 'Main carousel'),
    ('2', 'Right top adv'),
    ('3', 'Right middle adv'),
    ('4', 'Right bottom adv'),
    ('5', 'JS positon'),
)

BANNER_SOURCES = (
    ('1', 'From banner`s image'),
    ('2', 'From file'),
    ('3', 'From post`s title image'),
    ('4', 'From jscode'),
)

BANNER_PAGES = (
    ('1', 'Main page'),
    ('2', 'Gategory selected'),
    ('3', 'Region selected'),
    ('4', 'Other pages'),
)

# Assumes that banners html render would be depends on type of banner.
# Still not realized.
BANNER_TYPES = (
    ('1', 'jpg/gif'),
    ('2', 'JS banner'),
    ('3', 'Flash')
)

FEEDBACK_MAIL = 'journal.welcome@mail.ru'

HTML_TITLE_CATEGORY = {
    u'Shops': 'Sport shops and Rent equipment',
    u'Events': 'Events',
    u'Hotels': 'Hotels and Hostels',
    u'Restaurants': 'Restaurants and Cafes',
    u'Pubs': 'Bars and Pubs',
    u'Coffees': 'Coffee houses and Tea houses',
    u'Skis': 'Ski bases',
    u'Tours': 'Tours and Travel Agency',
    u'Entertainments': 'Entertainments',
}

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.sitemaps',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'redactor',
    'blog',
    'south',
    'photologue',
    'admin_enhancer',
    'multiselectfield',
    'django_select2',
)

MANAGERS = ADMINS

MEDIA = 'uploads/'
MEDIA_ROOT = os.path.join(BASE_DIR, MEDIA)
MEDIA_URL = '/'+MEDIA

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)

PAGINATOR_ONPAGE_NUMBERS_DAFAULT = 6
PAGINATOR_ONPAGE_NUMBERS_EVENTS = 8

PHOTOLOGUE_DIR = 'photologue/'


def get_photologue_path(instance, filename):
    ext = filename.split('.')[-1]
    filename = u'%s.%s' % (uuid4().hex, ext)
    return os.path.join(PHOTOLOGUE_DIR, 'photos', filename)

PHOTOLOGUE_PATH = get_photologue_path

PROJECT_SYS_NAME = 'kgguide'
PROJECT_STATIC_URL = PROJECT_SYS_NAME+'/'

REDACTOR_UPLOAD = ADMIN_WYSIWIG_UPLOADS_DIR
REDACTOR_OPTIONS = {
    'lang': 'en',
}

ROOT_URLCONF = 'kgguide.urls'

SELECT2_STATIC_PREFIX = 'select2/'

SESSION_SERIALIZER = 'django.contrib.sessions.serializers.JSONSerializer'

SITE_ID = 1
SITEMAP_CHANGE_FREQ = 'hourly'

POST_TYPES = (
    ('1', 'Any post'),
    ('2', 'Invisible'),
)

STATIC = 'static/'
STATIC_ROOT = os.path.join(BASE_DIR, STATIC)
STATIC_URL = '/'+STATIC

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, 'blog/templates/blog/'),
)

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

LANGUAGE_CODE = 'en-us'

# Use to set "default" ModelField's property only
LOCATIONTZ = timezone('Asia/Bishkek')

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s \
                       %(process)d %(thread)d %(funcName)s %(message)s'
        },
    },
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'logfile': {
            'level': 'DEBUG',
            'filters': ['require_debug_false'],
            'class': 'logging.handlers.WatchedFileHandler',
            'filename': os.path.join(
                os.path.dirname(os.path.dirname(BASE_DIR)),
                'logs/logfile.log'
            ),
            'formatter': 'verbose',
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins', 'logfile'],
            'level': 'ERROR',
            'propagate': True,
        },
        'logg': {
            'handlers': ['logfile'],
            'level': 'DEBUG',
            'propagate': True,
        },
    }
}

TIME_ZONE = 'UTC'

USE_TZ = False
USE_I18N = True
USE_L10N = True

WSGI_APPLICATION = 'kgguide.wsgi.application'

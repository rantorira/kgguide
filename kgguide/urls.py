# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin

from blog.views import (
    BlogSitemap, CategoriesRegionsView,
    DefaultView, DetailsView, OtherViewsAdvert,
    advert_redirect, home_page_redirect
)

from kgguide.views import BaseTestView, Uploads

admin.autodiscover()

sitemaps = {
    'blog': BlogSitemap(),
}

urlpatterns = patterns(
    '',
    url(r'^upload/wysiwyg_image/(?P<upload_to>.*)', Uploads().upload_image,
        {
            'response': lambda name, url:
            '<img src="%s" alt="%s" />' % (url, name)
        },
        name='redactor_upload_image'),
    url(r'^$', DefaultView.as_view(), name='blog_home_page'),
    url(r'^none/.*', advert_redirect),

    url(r'^(?P<query_page>advert)/$',
        OtherViewsAdvert.as_view(), name='advert_page'),
    url(r'^(?P<query_page>contact_us)/$',
        OtherViewsAdvert.as_view(), name='contact_us_page'),

    url(r'^admin/', include(admin.site.urls)),

    # hack that disabled(make redirect) all url_paths from photologue.urls
    url(r'^photologue.*', home_page_redirect),
    url(r'^photologue/', include('photologue.urls')),

    # hack that disabled(make redirect) all url_paths from redactor.urls
    url(r'^redactor.*', home_page_redirect),
    url(r'^redactor/', include('redactor.urls')),

    url(r'^sitemap.xml$', 'django.contrib.sitemaps.views.sitemap',
        {'sitemaps': sitemaps}),
    url(r'^test/', BaseTestView.as_view()),

    url(r'^(?P<category>[\-\d\w]+)/page-(?P<page>\d+)/$',
        CategoriesRegionsView.as_view(), name='category_pages_page'),
    url(r'^(?P<category>[\-\d\w]+)/$',
        CategoriesRegionsView.as_view(), name='category_page'),
    url(r'^(?P<category>[\-\d\w]+)/(?P<region>[\-\d\w]+)/page-(?P<page>\d+)/$',
        CategoriesRegionsView.as_view(), name='category_region_pages_page'),
    url(r'^(?P<category>[\-\d\w]+)/(?P<region>[\-\d\w]+)/$',
        CategoriesRegionsView.as_view(), name='category_region_page'),
    url((r'^(?P<category>[\-\d\w]+)/'
         r'(?P<region>[\-\d\w]+)/'
         r'(?P<post_slug_id>[\-\d\w]+)/$'),
        DetailsView.as_view(), name='post_details_page'),
    )

if (not settings.PROD):
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
    # runsevrer should be run with --nostatic option
    # ./manage.py runserver --nostatic
    urlpatterns += static(settings.STATIC_URL,
                          document_root=settings.STATIC_ROOT)

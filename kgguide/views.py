# -*- coding: utf-8 -*-

import os
import re
# import logging
from datetime import datetime

from django.conf import settings
from django.utils.timezone import now
from django.views.generic.base import TemplateView
from redactor.views import redactor_upload


class Uploads():

    def upload_image(self, request, upload_to=None, **kwargs):
        # Set upload_to like current post_id
        if upload_to is not None:
            regex = re.compile(r'^(.*?)\/post\/(\d+)\/$')
            regres = regex.match(request.META['HTTP_REFERER'])
            if regres:
                upload_to = regres.group(2)

        uploads_dir = getattr(settings, 'ADMIN_WYSIWIG_UPLOADS_DIR')
        upload_to = os.path.join(uploads_dir, upload_to)
        return redactor_upload(request, upload_to, **kwargs)


# Dirty view for any ugly tests
class BaseTestView(TemplateView):
    template_name = "test.html"

    def get_context_data(self, **kwargs):
        print now().strftime('%Y-%m-%d %H:%M:%S')
        print datetime.now().strftime('%Y-%m-%d %H:%M:%S')

#        logger = logging.getLogger('logg')
#        deb = logger.debug
#        deb(now().strftime('%Y-%m-%d %H:%M:%S'))
#        deb(datetime.now().strftime('%Y-%m-%d %H:%M:%S'))

        context = super(BaseTestView, self).get_context_data(**kwargs)
        return context

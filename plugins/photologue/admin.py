from django.contrib import admin
from django import forms
from django.conf import settings
from blog.widgets import PhotologueAdminWidget

from .models import Gallery, Photo, GalleryUpload, PhotoEffect, PhotoSize, \
    Watermark

USE_CKEDITOR = getattr(settings, 'PHOTOLOGUE_USE_CKEDITOR', False)

if USE_CKEDITOR:
    from ckeditor.widgets import CKEditorWidget


class _BaseForm(object):
   def clean(self):
       for field in self.cleaned_data:
           if isinstance(self.cleaned_data[field], basestring):
               self.cleaned_data[field] = self.cleaned_data[field].strip()
       return self.cleaned_data


class GalleryAdminForm(forms.ModelForm):
    if USE_CKEDITOR:
        description = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Gallery


class PhotoInlineForm(forms.ModelForm):

    class Meta:
        model = Photo
        widgets = {
            'image' : PhotologueAdminWidget(),
        }

    def __init__(self, *args, **kwargs):
        self.form_instance = kwargs.get('instance', None)
        super(PhotoInlineForm, self).__init__(*args, **kwargs)
        self.fields['image'].widget.form_instance = getattr(self, 'form_instance')


class PhotoInline(admin.TabularInline):
    model = Photo
    form = PhotoInlineForm


class GalleryAdmin(admin.ModelAdmin):
    list_display = ('title', 'date_added', 'photo_count', 'is_public')
    list_filter = ['date_added', 'is_public']
    date_hierarchy = 'date_added'
    prepopulated_fields = {'title_slug': ('title',)}
#    filter_horizontal = ('photos',)
    inlines = [
            PhotoInline,
    ]

    form = GalleryAdminForm


class PhotoAdminForm(_BaseForm,forms.ModelForm):
    if USE_CKEDITOR:
        caption = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Photo
        widgets = {
                'image': PhotologueAdminWidget(),
        }

    def __init__(self, *args, **kwargs):
        self.form_instance = kwargs.get('instance', None)
        super(PhotoAdminForm, self).__init__(*args, **kwargs)
        self.fields['image'].widget.form_instance = getattr(self, 'form_instance')

class PhotoAdmin(admin.ModelAdmin):
#    list_display = ('title', 'date_taken', 'date_added', 'is_public', 'tags', 'view_count', 'admin_thumbnail', 'is_title_image', 'get_gallery_name')
    list_display = ('title', 'date_taken', 'date_added', 'is_public', 'view_count', 'admin_thumbnail', 'is_title_image', 'get_gallery_name')
    list_filter = ['date_added', 'is_public']
    search_fields = ['title', 'title_slug', 'caption']
    list_per_page = 20
    prepopulated_fields = {'title_slug': ('title',)}

    def get_gallery_name(self, obj):
#        galleries = obj.galleries.all()[:1]
#        for gallery in galleries:
#            return '%s'%(gallery.title)
        if (obj.gallery is not None):
            return '%s'%(obj.gallery.title)

    get_gallery_name.short_description = 'Gallery'

    form = PhotoAdminForm


class PhotoEffectAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'color', 'brightness', 'contrast', 'sharpness', 'filters', 'admin_sample')
    fieldsets = (
        (None, {
            'fields': ('name', 'description')
        }),
        ('Adjustments', {
            'fields': ('color', 'brightness', 'contrast', 'sharpness')
        }),
        ('Filters', {
            'fields': ('filters',)
        }),
        ('Reflection', {
            'fields': ('reflection_size', 'reflection_strength', 'background_color')
        }),
        ('Transpose', {
            'fields': ('transpose_method',)
        }),
    )


class PhotoSizeAdmin(admin.ModelAdmin):
    list_display = ('name', 'width', 'height', 'crop', 'pre_cache', 'effect', 'increment_count')
    fieldsets = (
        (None, {
            'fields': ('name', 'width', 'height', 'quality')
        }),
        ('Options', {
            'fields': ('upscale', 'crop', 'pre_cache', 'increment_count')
        }),
        ('Enhancements', {
            'fields': ('effect', 'watermark',)
        }),
    )


class WatermarkAdmin(admin.ModelAdmin):
    list_display = ('name', 'opacity', 'style')


class GalleryUploadAdmin(admin.ModelAdmin):

    def has_change_permission(self, request, obj=None):
        return False  # To remove the 'Save and continue editing' button


admin.site.register(Gallery, GalleryAdmin)
admin.site.register(GalleryUpload, GalleryUploadAdmin)
admin.site.register(Photo, PhotoAdmin)
admin.site.register(PhotoEffect, PhotoEffectAdmin)
admin.site.register(PhotoSize, PhotoSizeAdmin)
admin.site.register(Watermark, WatermarkAdmin)

# -*- coding: utf-8 -*-

import re

from admin_enhancer.admin import EnhancedModelAdminMixin

from django import forms
from django.conf import settings
from django.contrib import admin
from django_select2.widgets import Select2Widget

from photologue.models import Photo, Gallery
from redactor.widgets import RedactorEditor

from blog.models import Post, Category, Region, Banner, Place
from blog.widgets import CharsLeftInput, PhotologueAdminWidget


class _BaseForm(object):
    def clean(self):
        for field in self.cleaned_data:
            if isinstance(self.cleaned_data[field], basestring):
                self.cleaned_data[field] = self.cleaned_data[field].strip()
        return self.cleaned_data


class AdminPhonesField(forms.CharField):
    def __init__(self, *args, **kwargs):
        super(AdminPhonesField, self).__init__(*args, **kwargs)

    def validate(self, value):
        super(AdminPhonesField, self).validate(value)

        errors = []
        for phone in value.split(','):
            prep_val = phone.strip()
            if not re.match('\+\d{1,3}\s\(\d{1,5}\)\s\d{1,3}\-\d{2}\-\d{2}',
                            prep_val):
                errors.append(
                    forms.ValidationError(
                        'Invalid phone number: %(value)s',
                        code='invalid',
                        params={'value': prep_val},
                    )
                )
        if errors:
            raise forms.ValidationError(errors)


class PostAdminForm(_BaseForm, forms.ModelForm):
    phone = AdminPhonesField(help_text=('Number in international format.'
                                        'FE: +123 (456) 77-88-99, ...'))

    def __init__(self, *args, **kwargs):
        super(PostAdminForm, self).__init__(*args, **kwargs)
        self.fields['place'].queryset = Place.objects.all()
        self.fields['place'].label_from_instance = \
            lambda obj: "%s (%s)" % (obj.title, obj.address)

        self.fields['email'].help_text = "Use ',' as splitter"

    class Meta:
        model = Post
        widgets = {
            'content': RedactorEditor(),
            'short_description': CharsLeftInput(),
            'place': Select2Widget(select2_options={
                'width': '250px',
                'minimumResultsForSearch': 10,
                'closeOnSelect': True,
            }),
            'gallery': Select2Widget(select2_options={
                'width': '250px',
                'minimumResultsForSearch': 10,
                'closeOnSelect': True,
            }),

        }


class PostAdmin(EnhancedModelAdminMixin, admin.ModelAdmin):
    list_display = ('title', 'pub_time_format', 'is_publish',
                    'event_start_date_format', 'event_end_date_format',
                    'category', 'region', 'ptype')
    prepopulated_fields = {'title_slug': ('title',)}
    form = PostAdminForm

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "gallery":
            kwargs["queryset"] = Gallery.objects.filter().order_by('title')

        return super(PostAdmin, self).formfield_for_foreignkey(
            db_field, request, **kwargs
        )

    def event_start_date_format(self, obj):
        if obj.event_start_date:
            return obj.event_start_date.strftime('%Y-%m-%d %H:%M:%S')
        return ''

    event_start_date_format.short_description = 'beginnig date'

    def event_end_date_format(self, obj):

        if obj.event_end_date:
            return obj.event_end_date.strftime('%Y-%m-%d %H:%M:%S')
        return ''

    event_end_date_format.short_description = 'pub dateend'

    def pub_time_format(self, obj):
        if obj.pub_time:
            return obj.pub_time.strftime('%Y-%m-%d %H:%M:%S')
        return ''

    pub_time_format.short_description = 'add date'

    class Media:
        PSN = getattr(settings, 'PROJECT_SYS_NAME')
        js = [
            '//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js',
            PSN+'/js/jquery-cookie-master/jquery.cookie.js',
            PSN+'/js/main.js',
        ]


class PhotoInlineForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.form_instance = kwargs.get('instance', None)
        super(PhotoInlineForm, self).__init__(*args, **kwargs)
        self.fields['image'].widget.form_instance = getattr(
            self, 'form_instance'
        )

    class Meta:
        model = Photo
        widgets = {
            'image': PhotologueAdminWidget(),
        }


class PhotoInline(admin.TabularInline):
    model = Photo
    form = PhotoInlineForm
    extra = 1


class BannerAdmin(admin.ModelAdmin):
    inlines = [
        PhotoInline,
    ]


class CategoryAdmin(admin.ModelAdmin):
    pass


class RegionAdmin(admin.ModelAdmin):
    pass


class PlaceAdmin(admin.ModelAdmin):
    pass


admin.site.register(Post, PostAdmin)
admin.site.register(Banner, BannerAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Region, RegionAdmin)
admin.site.register(Place, PlaceAdmin)

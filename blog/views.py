# -*- coding: utf-8 -*-

import re

from datetime import datetime

from django.conf import settings
from django.contrib import sitemaps
from django.contrib.sites.models import Site
from django.core.paginator import Paginator, EmptyPage
from django.core.urlresolvers import reverse
from django.http import Http404
from django.shortcuts import redirect
from django.template.defaultfilters import title, lower
from django.views.generic.base import TemplateView

from blog.models import Post, Category, Region, Banner


class BaseView(TemplateView):

    def get_context_data(self, **kwargs):
        context = super(BaseView, self).get_context_data(**kwargs)

        try:
            context['adverb_plase_1'] = Banner.objects.get(
                is_public=True, position="2")
        except:
            context['adverb_plase_1'] = False

        try:
            context['adverb_plase_2'] = Banner.objects.get(
                is_public=True, position="3")
        except:
            context['adverb_plase_2'] = False

        try:
            context['adverb_plase_3'] = Banner.objects.get(
                is_public=True, position="4")
        except:
            context['adverb_plase_3'] = False

        try:
            context['main_carousel_banner'] = Banner.objects.get(
                is_public=True, position="1")
        except:
            context['main_carousel_banner'] = False

        context['domain'] = Site.objects.get_current().domain
        context['FEEDBACK_MAIL'] = settings.FEEDBACK_MAIL

        return context


class DefaultView(BaseView):
    template_name = "default.html"

    def get_context_data(self, **kwargs):
        category = Category.objects.get(title="Events")
        self.category = category
        context = super(DefaultView, self).get_context_data(**kwargs)

        context['posts_list'] = Post.objects.filter(
            is_publish=True, category=category.id,
            event_end_date__gt=datetime.now()
        ).order_by('event_start_date')[:6]

        return context


class CategoriesRegionsView(BaseView):

    def get_context_data(self, **kwargs):
        try:
            category = Category.objects.get(
                title=title(self.kwargs.get('category', ''))
            )
        except:
            Error404().get_default_django404()

        self.category = category
        self.region = self.kwargs.get('region', '')
        self.page = self.kwargs.get('page', '')

        # will NO! replace super.template_name
        self.template_name = "CategoryContent.html"
        context = super(
            CategoriesRegionsView, self
        ).get_context_data(**kwargs)

        context['html_title_category'] = getattr(
            settings, 'HTML_TITLE_CATEGORY'
        )[category.title]

        filter_params = dict(
            is_publish=True,
            category=category.id,
            event_end_date__gt=datetime.now()
        )

        if self.region != '':
            try:
                region = Region.objects.get(title=title(self.region))
            except:
                Error404().get_default_django404()

            posts_list = region.post_set.filter(
                **filter_params
            ).order_by('event_start_date')
        else:
            posts_list = Post.objects.filter(
                **filter_params
            ).order_by('event_start_date')

        regions_id = list(
            Post.objects.filter(
                **filter_params
            ).values_list("region_id", flat=True)
        )

        context['stored_regions_list'] = Region.objects.filter(
            id__in=set(regions_id)
        ).values("title")

        paginator = Paginator(
            posts_list,
            getattr(
                settings, 'PAGINATOR_ONPAGE_NUMBERS_'+category.title.upper(),
                getattr(settings, 'PAGINATOR_ONPAGE_NUMBERS_DAFAULT')
            ),
            allow_empty_first_page=True
        )

        # allways has the first page
        context['posts_pages'] = paginator.page_range

        if self.page:
            try:
                posts_page = paginator.page(self.page)
            except EmptyPage:
                posts_page = paginator.page(paginator.num_pages)

            context['current_page'] = posts_page.number
            context['posts_list'] = posts_page
        else:
            context['current_page'] = 0
            context['posts_list'] = posts_list

        return context


class DetailsView(CategoriesRegionsView):

    def get(self, request, **kwargs):
        self.request = request

        regex = re.compile(r'^([\-\d\w]+)\-(\d+)$', re.UNICODE)
        regres = regex.match(self.kwargs.get('post_slug_id', ''))
        self.title_slug, self.post_id = None, None

        if regres:
            self.title_slug = regres.group(1)
            self.post_id = regres.group(2)

        try:
            self.Post = Post.objects.get(pk=self.post_id, is_publish=True)
        except:
            Error404().get_default_django404()

        if self.Post.title_slug != self.title_slug:
            return correct_slug_redirect(self)

        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)

    def get_context_data(self, **kwargs):

        context = super(DetailsView, self).get_context_data(**kwargs)

        # will replace super.template_name
        self.template_name = "%s_details.html" % lower(self.category)

        context['post_details'] = self.Post

        return context


class OtherViewsAdvert(DefaultView):

    def get_context_data(self, **kwargs):
        self.query_page = self.kwargs.get('query_page', 'contact_us')
        context = super(OtherViewsAdvert, self).get_context_data(**kwargs)

        self.template_name = "%s.html" % self.query_page

        return context


class BlogSitemap(sitemaps.Sitemap):

    changefreq = settings.SITEMAP_CHANGE_FREQ
    priority = 0.5

    def items(self):
        return Post.objects.filter(is_publish=True)

    def lastmod(self, obj):
        return obj.pub_time


class Error404(object):

    def get_default_django404(self, *args, **kwargs):
        print "get_default_django404:"
        print "self ->"
        print self
        print "args ->"
        print args
        print "kwargs ->"
        print kwargs
        print "<- Error:)"
        raise Http404


def home_page_redirect(request):
    return redirect(reverse('blog_home_page'))


def advert_redirect(request):
    return redirect(reverse('advert_page', kwargs={'query_page': 'advert'}))


def correct_slug_redirect(self):
    return redirect(self.Post.get_absolute_url())

# -*- coding: utf-8 -*-

from datetime import datetime
from urlparse import urljoin

from django.db import models
from django.conf import settings
from django.core.urlresolvers import reverse
from django.core.validators import validate_email
from django.template.defaultfilters import lower
from django.utils.translation import ugettext_lazy as _

from multiselectfield import MultiSelectField

from south.modelsinspector import add_introspection_rules

from blog.utils import slugify

LOCATIONTZ = getattr(settings, 'LOCATIONTZ')
POST_TYPES = getattr(settings, 'POST_TYPES')
BANNER_POSITIONS = getattr(settings, 'BANNER_POSITIONS')
BANNER_SOURCES = getattr(settings, 'BANNER_SOURCES')
BANNER_PAGES = getattr(settings, 'BANNER_PAGES')
BANNER_TYPES = getattr(settings, 'BANNER_TYPES')

# Reserved for seettings
BANNER_INCORRECT_TARGET_URL = None

add_introspection_rules([], ["^blog\.models\.PostEmailsField"])


class Banner(models.Model):
    is_public = models.BooleanField(_('is public'), default=True, blank=True)
    title = models.CharField(
        max_length=255,
        default=datetime.now(LOCATIONTZ).strftime('%Y-%m-%d %H:%M:%S'),
        blank=True, null=True
    )
    position = models.CharField(
        max_length=2,
        choices=BANNER_POSITIONS,
        default=1, blank=True, null=True
    )
    pages = MultiSelectField(choices=BANNER_PAGES, default=1)
    # Assumes that banners html render would be depends on type of banner.
    btype = models.CharField(
        max_length=2,
        choices=BANNER_TYPES,
        default=1, blank=True, null=True
    )
    source_type = models.CharField(
        max_length=2,
        choices=BANNER_SOURCES,
        default=1, blank=True, null=True
    )
    target_url = models.CharField(max_length=255, blank=True, null=True)
    file_source = models.FileField(
        upload_to='banners/',
        max_length=255,
        blank=True,
        null=True
    )
    jscode_source = models.TextField(max_length=10000, blank=True, null=True)

    def get_target_url(self):
        if self.target_url:
            return self.target_url
        else:
            try:
                return self.post.get_absolute_url()
            except:
                # Still not realized.
                return BANNER_INCORRECT_TARGET_URL or '/page404/'

    def get_source(self):
        if self.source_type == '1':
            return map(self._get_photologue_image_url,
                       self.photos.filter(is_public=True))
        elif self.source_type == '2':
            return [
                urljoin(settings.MEDIA_URL, self.file_source)
            ]
        elif self.source_type == '3':
            return [
                self._get_photologue_image_url(
                    self.post.get_title_photo()
                )
            ]
        elif self.source_type == '4':
            return [self.jscode_source]

    def _get_photologue_image_url(self, photo):
        if self.position == '1':
            return photo.get_main_carousel_url()
        elif self.position == '2':
            return photo.get_right_banner_top_url()
        elif self.position == '3':
            return photo.get_right_banner_middle_url()
        elif self.position == '4':
            return photo.get_right_banner_bottom_url()

    def __unicode__(self):
        return u'%s' % self.title


class PostEmailsField(models.CharField):

    def validate(self, value, model_instance):
        super(PostEmailsField, self).validate(value, self)

        for email in value.split(','):
            prep_val = email.encode('utf-8').strip()
            validate_email(prep_val)


class Post(models.Model):
    is_publish = models.BooleanField()
    pub_time = models.DateTimeField(
        u'Дата публикации',
        default=datetime.now(LOCATIONTZ),
        blank=True, null=True
    )
    title = models.CharField(
        max_length=255,
        default=datetime.now(LOCATIONTZ).strftime('%Y-%m-%d %H:%M:%S'),
        blank=True, null=True
    )
    title_slug = models.CharField(
        _('slug'),
        max_length=250,
        unique=True, blank=True, null=True
    )
    category = models.ForeignKey('Category', blank=True, null=True)
    region = models.ForeignKey('Region', blank=True, null=True)
    ptype = models.CharField(
        max_length=2,
        choices=POST_TYPES,
        default=1, blank=True, null=True
    )
    event_start_date = models.DateTimeField(
        u'Дата события',
        blank=True, null=True
    )
    event_end_date = models.DateTimeField(
        u'Дата окончания публикации',
        blank=True, null=True
    )
    short_description = models.CharField(
        max_length=255,
        blank=True, null=True
    )
    content = models.TextField(max_length=10000, blank=True, null=True)
    place = models.ForeignKey('Place', blank=True, null=True)
    address = models.CharField(max_length=255, blank=True, null=True)
    email = PostEmailsField(max_length=255, blank=True, null=True)
    phone = models.CharField(max_length=255, blank=True, null=True)
    additional_info = models.TextField(max_length=500, blank=True, null=True)
    gallery = models.ForeignKey('photologue.Gallery', blank=True, null=True)
    banner = models.OneToOneField(Banner, blank=True, null=True)

    def __unicode__(self):
        return u'%s' % self.title

    def get_absolute_url(self):
        return reverse('post_details_page', kwargs={
            'category': lower(self.category),
            'region': lower(self.region),
            'post_slug_id': "%s-%i" % (self.title_slug, self.id)
        })

    def get_title_photo(self):
        return self.gallery.photos.get(is_title_image=True)

    def get_all_photos(self):
        return self.gallery.photos.filter(is_public=True)

    def get_address(self):
        if self.address:
            return self.address
        elif self.place:
            return self.place.address
        else:
            return None

    def save(self, *args, **kwargs):
        if self.title_slug is None or not self.title_slug:
            self.title_slug = slugify(self.title)

        if self.event_end_date is None or not self.event_end_date:
            if self.event_start_date is None or not self.event_start_date:
                self.event_end_date = self.pub_time
            else:
                self.event_end_date = self.event_start_date

        super(Post, self).save(*args, **kwargs)


class Category(models.Model):
    title = models.CharField(max_length=255, null=True)

    def __unicode__(self):
        return u'%s' % self.title


class Region(models.Model):
    title = models.CharField(max_length=255, null=True)

    def __unicode__(self):
        return u'%s' % self.title


class Place(models.Model):
    title = models.CharField(max_length=255, null=True)
    latitude = models.CharField(max_length=255, null=True, blank=True)
    longitude = models.CharField(max_length=255, null=True, blank=True)
    address = models.CharField(max_length=255, null=True, blank=True)

    def __unicode__(self):
        return u'%s' % self.title

# -*- coding: utf-8 -*-

from django import forms

from django.forms import widgets

from django.utils.encoding import force_unicode
from django.utils.safestring import mark_safe


class PhotologueAdminWidget(widgets.FileInput):

    def __init__(self, *args, **kwargs):
        super(PhotologueAdminWidget, self).__init__(*args, **kwargs)

    def render(self, name, value, attrs=None):
        output = []
        if (self.form_instance is not None):
            output.append(
                '<a target="_blank" href="%s"><img src="%s"/></a>\n' % (
                    self.form_instance.get_thumbnail_url(),
                    self.form_instance.get_thumbnail_url()
                )
            )
        else:
            pass
        output.append(
            super(
                PhotologueAdminWidget, self
            ).render(name, value, attrs)
        )
        return mark_safe(u''.join(output))


class MediaMixin(object):
    pass

    class Media:
        css = {'all': ('charsleft-widget/css/charsleft.css',)}
        js = ('charsleft-widget/js/charsleft.js',)


class CharsLeftInput(forms.Textarea, MediaMixin):

    def render(self, name, value, attrs=None, *args, **kwargs):
        if value is None:
            value = ''
        maxlength = self.attrs.get('maxlength', 255)

        remaining = force_unicode(int(maxlength) - len(value))
        output = []
        output.append('<span class="charsleft charsleft-input">\n')
        output.append(super(CharsLeftInput, self).render(name, value, attrs))
        output.append(("""\n<span><span class="count">%s</span> characters remaining</span>
            <span class="maxlength">%s</span>
            </span>""") % (remaining, int(maxlength)))

        return mark_safe(u''.join(output))

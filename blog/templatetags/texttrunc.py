import re
from django import template

register = template.Library()

TAG_END_RE = re.compile(r'(\w+)[^>]*>')
ENTITY_END_RE = re.compile(r'(\w+;)')


@register.filter
def truncatehtml(string, length, ellipsis='...'):
    if (string is None or not string):
        return "..."

    """
    Truncate HTML string, preserving tag structure and character entities.
    """

    length = int(length)
    output_length = 0
    i = 0
    pending_close_tags = {}

    while output_length < length and i < len(string):
        c = string[i]

        if c == '<':
            # probably some kind of tag
            if i in pending_close_tags:
                # just pop and skip if it's closing tag we already knew about
                i += len(pending_close_tags.pop(i))
            else:
                # else maybe add tag
                i += 1
                match = TAG_END_RE.match(string[i:])
                if match:
                    tag = match.groups()[0]
                    i += match.end()

                    # save the end tag for possible later use if there is one
                    match = re.search(r'(</' + tag + '[^>]*>)',
                                      string[i:], re.IGNORECASE)
                    if match:
                        pending_close_tags[
                            i + match.start()
                        ] = match.groups()[0]
                else:
                    # some kind of garbage, but count it in
                    output_length += 1

        elif c == '&':
            # possible character entity, we need to skip it
            i += 1
            match = ENTITY_END_RE.match(string[i:])
            if match:
                i += match.end()

            # this is either a weird character or just '&', both count as 1
            output_length += 1
        else:
            # plain old characters

            skip_to = string.find('<', i, i + length)
            if skip_to == -1:
                skip_to = string.find('&', i, i + length)
            if skip_to == -1:
                skip_to = i + length

            # clamp
            delta = min(skip_to - i,
                        length - output_length,
                        len(string) - i)

            output_length += delta
            i += delta

    output = [string[:i]]
    if output_length == length:
        output.append(ellipsis)

    for k in sorted(pending_close_tags.keys()):
        output.append(pending_close_tags[k])

    return "".join(output)


truncatehtml.is_safe = True


class SetVarNode(template.Node):

    def __init__(self, new_val, var_name):
        self.new_val = new_val
        self.var_name = var_name

    def render(self, context):
        context[self.var_name] = self.new_val
        print context[self.var_name]
        return ''


@register.tag
def setvar(parser, token):
    # This version uses a regular expression to parse tag contents.
    try:
        # Splitting by None == splitting by spaces.
        tag_name, arg = token.contents.split(None, 1)
    except ValueError:
        raise (template.TemplateSyntaxError,
               "%r tag requires arguments" % token.contents.split()[0])
    m = re.search(r'(.*?) as (\w+)', arg)
    if not m:
        raise (template.TemplateSyntaxError,
               "%r tag had invalid arguments" % tag_name)
    new_val, var_name = m.groups()
    if not (new_val[0] == new_val[-1] and new_val[0] in ('"', "'")):
        raise (template.TemplateSyntaxError,
               "%r tag's argument should be in quotes" % tag_name)
    return SetVarNode(new_val[1:-1], var_name)

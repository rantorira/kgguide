from django import template
from django.conf import settings
from django.templatetags.static import StaticNode
from django.utils.six.moves.urllib.parse import urljoin

register = template.Library()


class ProjectStaticNode(StaticNode):

    def handle_simple(self, path):
        prefix = urljoin(settings.STATIC_URL, settings.PROJECT_STATIC_URL)
        return urljoin(prefix, path)

"""
Has only one difference from django's native 'static' tag.
It is adding PROJECT_STATIC_URL to STATIC_URL

Examples::

    PROJECT_STATIC_URL = 'myapp'
    STATIC_URL = '/static/'

    # projectstatic.projstatic:
    {% projstatic "css/base.css" %}
    # return: /static/myapp/css/base.css


    # django's native:
    {% static "myapp/css/base.css" %}
    # return: /static/myapp/css/base.css

"""


@register.tag('projstatic')
def do_static(parser, token):
    return ProjectStaticNode.handle_token(parser, token)

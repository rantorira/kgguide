# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Banner'
        db.create_table(u'blog_banner', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('is_public', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('title', self.gf('django.db.models.fields.CharField')(default='2014-04-30 21:33:33', max_length=255, null=True, blank=True)),
            ('position', self.gf('django.db.models.fields.CharField')(default=1, max_length=2, null=True, blank=True)),
            ('pages', self.gf('multiselectfield.db.fields.MultiSelectField')(default=1, max_length=7)),
            ('btype', self.gf('django.db.models.fields.CharField')(default=1, max_length=2, null=True, blank=True)),
            ('source_type', self.gf('django.db.models.fields.CharField')(default=1, max_length=2, null=True, blank=True)),
            ('target_url', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('file_source', self.gf('django.db.models.fields.files.FileField')(max_length=255, null=True, blank=True)),
            ('jscode_source', self.gf('django.db.models.fields.TextField')(max_length=10000, null=True, blank=True)),
        ))
        db.send_create_signal(u'blog', ['Banner'])

        # Adding model 'Post'
        db.create_table(u'blog_post', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('is_publish', self.gf('django.db.models.fields.BooleanField')()),
            ('pub_time', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2014, 4, 30, 0, 0), null=True, blank=True)),
            ('title', self.gf('django.db.models.fields.CharField')(default='2014-04-30 21:33:33', max_length=255, null=True, blank=True)),
            ('title_slug', self.gf('django.db.models.fields.CharField')(max_length=250, unique=True, null=True, blank=True)),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['blog.Category'], null=True, blank=True)),
            ('region', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['blog.Region'], null=True, blank=True)),
            ('ptype', self.gf('django.db.models.fields.CharField')(default=1, max_length=2, null=True, blank=True)),
            ('event_start_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('event_end_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('short_description', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('content', self.gf('django.db.models.fields.TextField')(max_length=10000, null=True, blank=True)),
            ('place', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['blog.Place'], null=True, blank=True)),
            ('address', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('email', self.gf('blog.models.PostEmailsField')(max_length=255, null=True, blank=True)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('additional_info', self.gf('django.db.models.fields.TextField')(max_length=500, null=True, blank=True)),
            ('gallery', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['photologue.Gallery'], null=True, blank=True)),
            ('banner', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['blog.Banner'], unique=True, null=True, blank=True)),
        ))
        db.send_create_signal(u'blog', ['Post'])

        # Adding model 'Category'
        db.create_table(u'blog_category', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
        ))
        db.send_create_signal(u'blog', ['Category'])

        # Adding model 'Region'
        db.create_table(u'blog_region', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
        ))
        db.send_create_signal(u'blog', ['Region'])

        # Adding model 'Place'
        db.create_table(u'blog_place', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('latitude', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('longitude', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('address', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
        ))
        db.send_create_signal(u'blog', ['Place'])


    def backwards(self, orm):
        # Deleting model 'Banner'
        db.delete_table(u'blog_banner')

        # Deleting model 'Post'
        db.delete_table(u'blog_post')

        # Deleting model 'Category'
        db.delete_table(u'blog_category')

        # Deleting model 'Region'
        db.delete_table(u'blog_region')

        # Deleting model 'Place'
        db.delete_table(u'blog_place')


    models = {
        u'blog.banner': {
            'Meta': {'object_name': 'Banner'},
            'btype': ('django.db.models.fields.CharField', [], {'default': '1', 'max_length': '2', 'null': 'True', 'blank': 'True'}),
            'file_source': ('django.db.models.fields.files.FileField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_public': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'jscode_source': ('django.db.models.fields.TextField', [], {'max_length': '10000', 'null': 'True', 'blank': 'True'}),
            'pages': ('multiselectfield.db.fields.MultiSelectField', [], {'default': '1', 'max_length': '7'}),
            'position': ('django.db.models.fields.CharField', [], {'default': '1', 'max_length': '2', 'null': 'True', 'blank': 'True'}),
            'source_type': ('django.db.models.fields.CharField', [], {'default': '1', 'max_length': '2', 'null': 'True', 'blank': 'True'}),
            'target_url': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'default': "'2014-04-30 21:33:33'", 'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'blog.category': {
            'Meta': {'object_name': 'Category'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'})
        },
        u'blog.place': {
            'Meta': {'object_name': 'Place'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'latitude': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'longitude': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'})
        },
        u'blog.post': {
            'Meta': {'object_name': 'Post'},
            'additional_info': ('django.db.models.fields.TextField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'address': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'banner': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['blog.Banner']", 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['blog.Category']", 'null': 'True', 'blank': 'True'}),
            'content': ('django.db.models.fields.TextField', [], {'max_length': '10000', 'null': 'True', 'blank': 'True'}),
            'email': ('blog.models.PostEmailsField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'event_end_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'event_start_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'gallery': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['photologue.Gallery']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_publish': ('django.db.models.fields.BooleanField', [], {}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'place': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['blog.Place']", 'null': 'True', 'blank': 'True'}),
            'ptype': ('django.db.models.fields.CharField', [], {'default': '1', 'max_length': '2', 'null': 'True', 'blank': 'True'}),
            'pub_time': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 4, 30, 0, 0)', 'null': 'True', 'blank': 'True'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['blog.Region']", 'null': 'True', 'blank': 'True'}),
            'short_description': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'default': "'2014-04-30 21:33:33'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'title_slug': ('django.db.models.fields.CharField', [], {'max_length': '250', 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        u'blog.region': {
            'Meta': {'object_name': 'Region'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'})
        },
        u'photologue.gallery': {
            'Meta': {'ordering': "['-date_added']", 'object_name': 'Gallery'},
            'date_added': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_public': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'title_slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'})
        }
    }

    complete_apps = ['blog']